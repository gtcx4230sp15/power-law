# Definitions #

Our course has been primarily about simulation, from assumed principles.

You’ll recall the contrast is data modeling, which is the subject of courses like CX 4240 and other statistics and statistical machine learning courses.

However, there is one type of data model that is usually not covered in those classes, yet so relevant to a wide variety of phenomena that I feel compelled to cover some aspects of it in this course.

That type of data model is the _power law distribution_. It’s kind of crazy that so many phenomena fall under its umbrella.

Let me start with the definition, which is simple to state. Consider some quantity $x$, such as the population of US cities. For simplicity, assume $x$ is continuous. Then, a random variable $X$ for this quantity follows a power law distribution if its probability density function (PDF) is given by

  $$p(x) = \frac{C}{x^\alpha} .$$

That’s it!

# What does it mean? #
So what does it mean? Start by comparing it to something else, like an exponential distribution:

  $$p_{\tiny\mbox{exp}}(x) = \frac{C_{\tiny\mbox{exp}}}{e^{\lambda x}} .$$

As you can see, an exponential decreases _much faster_ as $x$ increases. So, a power law distribution should have a much larger dynamic range. It is also much more skewed, in the sense that there is a lot of mass in the “tail” relative to the exponential. This fact has an interesting consequence: if you observe a sample, it will not be “well-behaved” in the sense that your estimates of the mean should be less confident. That is, a random sample will show a lot more variability in values than you would expect to see in, for instance, the exponential distribution.

That was the formal definition. More colloquially, people often say that a power law distribution is one in which a log-log plot of count versus the quantity is a straight line in the “tail” of the distribution. That follows from the definition of the PDF. Taking the log of the count in the interval $[x, x+dx)$, you’ll find that

  $$\log [p(x)dx] = -\alpha \log x + \log (\mbox{constant}).$$

That is a straight line in $\log x$, with a slope of negative $\alpha$.

The seminal introductory paper in this area is Mark Newman’s, “Power laws, Pareto distributions, and Zipf’s Law,” which was written in 2004. I’ve posted a link on Piazza, as usual. Most of what I will say now comes from this paper.

# Examples #

Figure 4 of Newman’s paper is remarkable. It gives 12 examples of power law distributions. These cover phenomena as diverse as word counts, citation counts, crater sizes, city populations, wealth, the frequency of names, the number of books sold, the magnitude of earthquakes, and on and on. Wow!

Interestingly, the empirically-estimated value of $\alpha$ for these distributions falls between roughly 1.8 and 3.5. Nine of them exceed 2.0, and four of them exceed 3.0.

Importantly, many things do not follow a power law, either. Figure 5 of the paper gives 3 examples: the abundance of bird species, the number of email addresses in a person’s contact list, and the area of forest fires. So, the point is that you shouldn’t assume power laws are everywhere — though you _should_ definitely learn to recognize them.

By the way, here are some links to raw data: <http://tuvalu.santafe.edu/~aaronc/powerlaws/data.htm>

# Minimum sizes #

An important point about power laws is that there is typically some minimum size above which the power law holds. You can see that from Figure 4, where there is often a minimum value of $x$ above which the power law form holds. Let’s call this value, $x_{\tiny\mbox{min}}$. So if we define the distribution so that it is only non-zero at $x > x_{\tiny\mbox{min}}$, then the proper normalized form of the PDF becomes,

$$p(x) = \frac{\alpha - 1}{x_{\tiny\mbox{min}}} \left(\frac{x_{\tiny\mbox{min}}}{x}\right)^\alpha.$$

With respect to the original definition above, the constant $C$ is,

$$C \equiv (\alpha - 1) x_{\tiny\mbox{min}}^{\alpha - 1}.$$


# Basic statistics: Moments and medians #

What about the mean and variance? Computing these yields some interesting consequences.

$$\bar{x} \equiv E[X] = \int_{x_{\tiny\mbox{min}}}^{\infty} x p(x) dx = \frac{C}{\alpha - 2} \left. \frac{1}{x^{\alpha-2}} \right|^{x_{\tiny\mbox{min}}}_{\infty} .$$

Notice that the mean is only defined when $\alpha > 2$, in which case the mean simplifies to the following:

$$\alpha > 2 \quad \Rightarrow \quad \bar{x} = \frac{\alpha - 1}{\alpha - 2} x_{\tiny\mbox{min}}.$$

The mean-square, which is related to the variance, is given by,

$$\bar{x^2} \equiv E[X^2] = \frac{C}{\alpha - 3} \left. \frac{1}{x^{\alpha-3}} \right|_\infty^{x_{\tiny\mbox{min}}}.$$

The mean-square only exists when $\alpha > 3$, in which case,

$$\alpha > 3 \quad \Rightarrow \quad \bar{x} = \frac{\alpha - 1}{\alpha - 3} x_{\tiny\mbox{min}}^2.$$

This pattern repeats for higher-order moments.

Besides the mean, you can also ask about the median, which you’ll recall is the value at which 50% of the distribution lies below and 50% is above:

$$x_{1/2} = 2^{\frac{1}{\alpha-1}} x_{\tiny\mbox{min}}.$$

Here’s another interesting quantity to estimate. Suppose you take $n$ samples from a power law distribution. What’s the expected largest value you will observe? A quick-and-dirty (albeit non-rigorous) way to compute this is as follows. Let

$$P(x) \equiv \int_{x}^{\infty} p(x)dx$$

be the _cumulative distribution function_, which in this definition computes the probability that $X \geq x$. (This is in contrast to the common definition, which considers $X \leq x$.) The largest value after $n$ samples is the one you expect to see  when $P(x) \approx \frac{1}{n}$, since the largest sample is the only one greater than $x$. Solving for $x$, you’ll find that,

$$x_{\tiny\mbox{max}} \approx n^{\frac{1}{\alpha-1}}.$$

For instance, suppose $\alpha=\frac{5}{2}=2.5$. Then $x_{\tiny\mbox{max}} \approx n^{\frac{2}{3}}$. Compare this expression to the one you would get for an exponential distribution, which would be proportional to $\ln n$, a much slower function.

# Top heaviness #

Another observation about power laws is that they tend to be top-heavy. Newman gives the example of the net wealth of Americans, one of the plots in Figure 4. You might ask, how much wealth is contained in the top half of the distribution? That is, how much cumulative wealth exists for all individuals whose income is the median value or greater? It happens that $\alpha \approx 2.09$. The answer turns out to be 94%! Talk about fat cats.


# The scale-free property #

A power-law distribution is _scale-free_. This means that it looks “the same” independent of how you scale it. More formally,

$$p(bx) = g(b)p(x)$$

for _any_ $b$. Put another way, you can take $x$ in some units, scale it to a different set of units, but the distribution has the same shape (always a line of the same slope).

Newman has a helpful example. Suppose file sizes follow a power law distribution, and suppose that 1 KiB files are 4 times more common than 2 KiB files. Then 1 MiB files will be 4 times more common than 2 MiB files. Same for GiB-sized files. (Recall that “KiB” is _Kibibytes_, which is $2^{10}=1024$ bytes; same for Mibibytes and Gibibytes.)

A cool fact is that a power law distribution is the _only_ scale-free distribution! You can show this fact from the definition above.

# The discrete case? #

It’s natural to attempt to define the discrete case by analogy to the continuous case, namely, by setting the probability mass function to have the form, $\sim k^{-\alpha}$, for each discrete value $k$. It has been suggested that a better generalization is something called the _Yule distribution_, due to Yule (1925). It is based on the _Legendre beta-function_. Refer to Newman’s paper for details. The Legendre beta-function has a limiting behavior that approaches $k^{-\alpha}$ as $k$ grows.


# Generating mechanisms #

The most interesting question we could ask is, how the heck to power law distributions come to be? In fact, there have been many proposals but no universal generating process. So it’s either an open question, or there is simply no _unique_ way to get there.

(TO BE CONTINUED)
