n = 1000;
p = 1 - 0.592746;
p = 1.05*p;
%p = 0.95*p;
n_trials = 100;

display ('running ...');
tic;
RegionSizes = [];
for i = 1:n_trials,
  World = (rand (n, n) <= p);
  ConnComp = bwconncomp (World); % Get connected components

  RegionSizes_i = cellfun (@(x) length (x), ConnComp.PixelIdxList);
  RegionSizes = [RegionSizes RegionSizes_i];
end
toc;

% plot
clf;
figure (1);
[N, X] = hist (log10 (RegionSizes));
loglog (10.^X, N / sum (N), '*:');
title (sprintf ('p=%f [%d x %d, %d trials]', p, n, n, n_trials));
axis square;
grid on;

% eof
