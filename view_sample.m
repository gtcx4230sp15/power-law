% process experimental results:
Data = csvread ('sample.csv', 1, 0);
[n_max, repetitions] = size (Data);
repetitions = repetitions - 1;

% Make histogram
max_count = max (max (Data(:, 2:repetitions+1)));
X = 0:.25:(ceil (log10 (max_count)));
N_all = [];
for r = 1:repetitions,
    N_r = hist (log10 (Data(:,1+r)), X);
    N_all = [N_all ; N_r];
end
N = sum (N_all);

% Filter out zero entries of the histogram
I_nz = (N > 0);
N = N(I_nz);
X = X(I_nz);
n = length (N);

% what theory predicts:
m = 3;
rho = 1.0 / (1 - 1/(m+1));
B = (1+rho) * beta (10.^X, 1+rho);

display ('=== frequency table ===');
sprintf ('%f\t%f\t%f\n', [10.^X; N/sum(N); B])
display ('=== ratios ===');
sprintf ('%f\t%f\n', [N(1:n-1) ./ N(2:n) ; B(1:n-1) ./ B(2:n)])

figure (1);
clf;
loglog (10.^X, N/sum(N), '*:', 10.^X, B, 'r-');
%axis equal;
decades_equal (gca);
grid on;
title ('frequencies');

% eof