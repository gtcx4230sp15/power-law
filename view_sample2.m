% process experimental results:
Data = csvread ('sample2.csv', 1, 0);

I_nz = (Data(:, 2) > 0);
Data_sorted = sortrows (Data (I_nz, :), 1);

% make a histogram
X = Data_sorted(:, 1)';
N = Data_sorted(:, 2)';
n = length (N);

% what theory predicts:
g = 0.25;
rho = 1.0 / (1.0 - g);
B = (1+rho) / (2 - g) * beta (X, 1+rho);

display ('=== frequency table ===');
sprintf ('%f\t%f\t%f\n', [X; N/sum(N); B])
display ('=== ratios ===');
sprintf ('%f\t%f\n', [N(1:n-1) ./ N(2:n) ; B(1:n-1) ./ B(2:n)])

figure (1);
clf;
loglog (X, N, '*:', X, sum(N) * B, 'r-');
%axis equal;
decades_equal (gca);
grid on;
title ('Counts');
xlabel ('Group size');
ax = axis;
axis ([ax(1:2) .1 ax(4)]);

% eof