#include <cassert>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <random>

using namespace std;

size_t
a_to_sizet (const char* s)
{
  long x_raw = atol (s); assert (x_raw > 0);
  return (size_t)x_raw;
}

template <typename T>
T
cumulativeSum (const vector<T>& X, vector<T>& S)
{
  const size_t n = X.size ();
  assert ((n+1) == S.size ());
  S[0] = 0;
  if (n >= 1) {
    for (size_t i = 1; i <= n; ++i) {
      S[i] = S[i-1] + X[i-1];
    }
  }
  return S[n];
}

template <typename T>
void
vectorAccumulate (vector<T>& Y, const vector<T>& X)
{
  const size_t n = Y.size ();
  assert (n <= X.size ());
  for (size_t i = 0; i < n; ++i)
    Y[i] += X[i];
}

template <typename T>
size_t
findBin__helper (const T& val, const vector<T>& bins, size_t i_min, size_t i_max)
{
  if (i_min >= i_max) return i_max;
  const size_t i_mid = (i_min + i_max) / 2;
  if (val >= bins[i_mid] && val < bins[i_mid+1]) return i_mid;
  if (val < bins[i_mid]) return findBin__helper (val, bins, i_min, i_mid);
  return findBin__helper (val, bins, i_mid+1, i_max);
}

template <typename T>
size_t
findBin (const T& val, const vector<T>& bins)
{
  const size_t n_bins = bins.size ();
  if (val <= bins[0]) return 0;
  if (val >= bins[n_bins-1]) return n_bins-1;
  return findBin__helper (val, bins, 0, n_bins);
}

void
simulate (size_t n_max, size_t m, vector<size_t>& genera_sizes)
{
  assert (n_max >= 1);
  
  vector<size_t> cumulative_sizes;
  cumulative_sizes.push_back (0);
  assert ((genera_sizes.size ()+1) == cumulative_sizes.size ()); // invariant to maintain

  // initial conditions
  size_t n = 1;
  genera_sizes.push_back (1);
  cumulative_sizes.push_back (0);
  cumulativeSum (genera_sizes, cumulative_sizes);

  uniform_real_distribution<double> unif(0.0, 1.0);
  default_random_engine re;
  
  while (n <= n_max) {
    const double create_new_genus = unif (re);
    if (create_new_genus < (1.0 / m)) {
      if (n == n_max) break;
      // create 1 new genus
      genera_sizes.push_back (1);
      cumulative_sizes.push_back (0);
      ++n;
    } else {
      const size_t total = cumulativeSum (genera_sizes, cumulative_sizes);
      const size_t p = ((size_t)random ()) % total;
      const size_t k = findBin (p, cumulative_sizes);
      assert (0 <= k && k < genera_sizes.size ());
      genera_sizes[k] += 1;
    } // s
  } // n
}

int
main (int argc, char* argv[])
{
  if (argc < 3) {
    cerr << "usage: " << argv[0] << " <m> <n> [repeats=1]" << endl;
    return 1;
  }

  const size_t m = a_to_sizet (argv[1]);
  const size_t n_max = a_to_sizet (argv[2]);
  const size_t repetitions = (argc >= 4) ? a_to_sizet (argv[3]) : 1;
  assert (repetitions >= 1);

  vector< vector<size_t> > all_sizes (repetitions);
  size_t r = 0; // repetitions
  do {
    cerr << "==> Trial: " << r+1 << " ...\r";
    
    vector<size_t> genera_sizes;
    simulate (n_max, m, genera_sizes);

    // combine this instance with global counts
    all_sizes[r].resize (genera_sizes.size ());
    copy (genera_sizes.begin (), genera_sizes.end (), all_sizes[r].begin ());
  } while ((++r) < repetitions);
  cerr << endl;

  // output header
  cout << "Genus";
  for (size_t r = 0; r < repetitions; ++r) cout << ",N" << r;
  cout << endl;

  // output body
  for (size_t i = 0; i < n_max; ++i) {
    cout << i;
    for (size_t r = 0; r < repetitions; ++r)
      cout << ',' << all_sizes[r][i];
    cout << endl;
  }
  
  return 0;
}

// eof
