#include <cassert>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <random>
#include <unordered_map>

using namespace std;

size_t
a_to_sizet (const char* s)
{
  long x_raw = atol (s); assert (x_raw > 0);
  return (size_t)x_raw;
}

template <typename T>
size_t
findBin__helper (const T& val, const vector<T>& bins, size_t i_min, size_t i_max)
{
  if (i_min >= i_max) return i_max;
  const size_t i_mid = (i_min + i_max) / 2;
  if (val >= bins[i_mid] && val < bins[i_mid+1]) return i_mid;
  if (val < bins[i_mid]) return findBin__helper (val, bins, i_min, i_mid);
  return findBin__helper (val, bins, i_mid+1, i_max);
}

template <typename T>
size_t
findBin (const T& val, const vector<T>& bins)
{
  const size_t n_bins = bins.size ();
  if (val <= bins[0]) return 0;
  if (val >= bins[n_bins-1]) return n_bins-1;
  return findBin__helper (val, bins, 0, n_bins);
}

typedef unordered_map<size_t, size_t> genera_dist_t;

void
buildProbabilityTable (genera_dist_t::const_iterator start,
		       genera_dist_t::const_iterator end,
		       vector<double>& prob_table)
{
  size_t n_genera = 0;
  double total = 0.0;
  for (genera_dist_t::const_iterator i = start; i != end; ++i) {
    const size_t groups_size = (i->first) * (i->second);
    total += groups_size;
    ++n_genera;
  }

  prob_table.resize (n_genera + 1);
  prob_table[0] = 0.0;
  size_t n = 1;
  for (genera_dist_t::const_iterator i = start; i != end; ++i, ++n) {
    const size_t groups_size = (i->first) * (i->second);
    prob_table[n] = prob_table[n-1] + (double)groups_size;
  }
  for (n = 0; n <= n_genera; ++n)
    prob_table[n] /= total;
}

size_t
selectWeightedRandomEntry (const vector<double>& prob_table,
			   const double draw)
{
  const size_t n = prob_table.size ();
  return findBin (draw, prob_table);
}

int
main (int argc, char* argv[])
{
  if (argc < 3) {
    cerr << "usage: " << argv[0] << " <t> <gamma>" << endl;
    return 1;
  }

  // process simulation parameters
  const size_t t_max = a_to_sizet (argv[1]);
  const double gamma = atof (argv[2]); assert (gamma > 0 && gamma < 1);

  cerr << ">> t_max == " << t_max << endl;
  cerr << ">> gamma == " << gamma << endl;

  // set initial conditions
  genera_dist_t genera_dist;
  genera_dist[1] = 1;

  // simulate
  cerr << "beginning simulation ..." << endl;
  vector<double> prob_table;
  uniform_real_distribution<double> unif(0.0, 1.0);
  default_random_engine re;
  for (size_t t = 2; t <= t_max; ++t) {
    const double create_new_genus = unif (re);
    if (!(t % 1000))
      cerr << "[t=" << t << "] " << create_new_genus << '\r';
    if (create_new_genus <= gamma) {
      genera_dist[1] += 1;
    } else {
      buildProbabilityTable (genera_dist.begin (), genera_dist.end (),
			     prob_table);
      size_t i = selectWeightedRandomEntry (prob_table, unif (re));
      size_t k = 0;
      for (genera_dist_t::const_iterator g = genera_dist.begin ();
	   g != genera_dist.end ();
	   ++g) {
	if (i == 0) k = g->first;
	i--;
      }
      genera_dist[k+1]++;
      genera_dist[k]--;
    }
  }
  cerr << endl;
  
  // output
  cout << "GenusSize,N" << endl; // header
  for (genera_dist_t::const_iterator i = genera_dist.begin ();
       i != genera_dist.end (); ++i) {
    cout << i->first << ',' << i->second << endl;
  }
  
  return 0;
}

// eof
